#ifndef __FIFGRAY_H__
#define __FIFGRAY_H__

#include "ti_msp_dl_config.h"


static uint8_t mask_line=0;

typedef struct
{
	uint32_t	 value;
	uint32_t line_GPIO_PIN;
}lineIDdef;


extern char have_line ;
extern void LineLedInit(void);
void taskFine_Line(uint32_t currentTick);
uint8_t LedFind_Scan(void);
int Find_Line_Begins(void);
uint8_t get_LedFind_Scan(void);



#endif
