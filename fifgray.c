#include "fifgray.h"

char have_line =0;
lineIDdef linepin_list[]=
{
	0,GPIO_GRP_GRAY_PIN_SENSER_1_PIN,
	0,GPIO_GRP_GRAY_PIN_SENSER_2_PIN,
	0,GPIO_GRP_GRAY_PIN_SENSER_3_PIN,
	0,GPIO_GRP_GRAY_PIN_SENSER_4_PIN,
	0,GPIO_GRP_GRAY_PIN_SENSER_5_PIN,
	0,GPIO_GRP_GRAY_PIN_SENSER_6_PIN,
	0,GPIO_GRP_GRAY_PIN_SENSER_7_PIN,
	0,GPIO_GRP_GRAY_PIN_SENSER_8_PIN,
};
extern void usart1_send_bytes(unsigned char *buf, int len);
void line_detect(void)
{

	for(uint8_t i=0;i<sizeof(linepin_list)/sizeof(linepin_list[0]);i++)
	{
		linepin_list[i].value = DL_GPIO_readPins(GPIOA,linepin_list[i].line_GPIO_PIN);
//		usart1_send_bytes((uint8_t*)(&linepin_list[i].value),4);
	}
	
}

void taskFine_Line(uint32_t currentTick)
{
				line_detect();	
}



uint8_t get_LedFind_Scan(void)
{
		if((linepin_list[7].value!=0)&&
		(linepin_list[6].value!=0)&&
		(linepin_list[5].value!=0)&&
		(linepin_list[4].value!=0)&&
		(linepin_list[3].value!=0)&&
		(linepin_list[2].value!=0)&&
		(linepin_list[1].value!=0)&&
		(linepin_list[0].value!=0))
		{
			have_line =0;
		}
		
		
		
		else  have_line =1;
		
	if(linepin_list[7].value == 0)
    return 7;
	else if(linepin_list[6].value == 0)
		return 6;
	else if(linepin_list[5].value == 0)
		return 5;
	else if(linepin_list[4].value == 0)
		return 4;
	else if(linepin_list[3].value == 0)
		return 3;
	else if(linepin_list[2].value == 0)
		return 2;
	else if(linepin_list[1].value == 0)
		return 1;
	else if(linepin_list[0].value == 0)
		return 0;
}

static int Linechange_list[8]=
{
	-4,
	-3,
	-2,
	-1,
	1,
	2,
	3,
	4
};
//extern void set_speed(uint16_t left,uint16_t right);
//char value_led[20];
//int Find_Line_Begins(void)
//{
//	static int find_value;
//	line_detect();
//	find_value = Linechange_list[get_LedFind_Scan()];

//  sprintf(value_led,"%d",find_value);
//  OLED_ShowString(0,2,value_led,8);
////	usart1_send_bytes(value_led,sizeof value_led);
//	set_speed(8  + find_value, 8  - find_value);

//}
