/**
 * These arguments were used when this file was generated. They will be automatically applied on subsequent loads
 * via the GUI or CLI. Run CLI with '--help' for additional information on how to override these arguments.
 * @cliArgs --device "MSPM0G350X" --package "LQFP-48(PT)" --part "Default" --product "mspm0_sdk@2.01.00.03"
 * @versions {"tool":"1.20.0+3587"}
 */

/**
 * Import the modules used in this configuration.
 */
const GPIO          = scripting.addModule("/ti/driverlib/GPIO", {}, false);
const GPIO1         = GPIO.addInstance();
const GPIO2         = GPIO.addInstance();
const GPIO3         = GPIO.addInstance();
const GPIO4         = GPIO.addInstance();
const GPIO5         = GPIO.addInstance();
const PWM           = scripting.addModule("/ti/driverlib/PWM", {}, false);
const PWM1          = PWM.addInstance();
const SYSCTL        = scripting.addModule("/ti/driverlib/SYSCTL");
const SYSTICK       = scripting.addModule("/ti/driverlib/SYSTICK");
const TIMER         = scripting.addModule("/ti/driverlib/TIMER", {}, false);
const TIMER1        = TIMER.addInstance();
const UART          = scripting.addModule("/ti/driverlib/UART", {}, false);
const UART1         = UART.addInstance();
const UART2         = UART.addInstance();
const ProjectConfig = scripting.addModule("/ti/project_config/ProjectConfig");

/**
 * Write custom configuration values to the imported modules.
 */
GPIO1.$name                              = "GPIO_GRP_MISC";
GPIO1.associatedPins.create(4);
GPIO1.associatedPins[0].$name            = "PIN_BEEP";
GPIO1.associatedPins[0].assignedPin      = "9";
GPIO1.associatedPins[0].pin.$assign      = "PB9";
GPIO1.associatedPins[1].$name            = "PIN_LED";
GPIO1.associatedPins[1].assignedPin      = "6";
GPIO1.associatedPins[1].initialValue     = "SET";
GPIO1.associatedPins[1].pin.$assign      = "PB6";
GPIO1.associatedPins[2].$name            = "PIN_KEY_1";
GPIO1.associatedPins[2].direction        = "INPUT";
GPIO1.associatedPins[2].internalResistor = "PULL_UP";
GPIO1.associatedPins[2].assignedPin      = "12";
GPIO1.associatedPins[2].pin.$assign      = "PA12";
GPIO1.associatedPins[3].$name            = "PIN_KEY_2";
GPIO1.associatedPins[3].direction        = "INPUT";
GPIO1.associatedPins[3].internalResistor = "PULL_UP";
GPIO1.associatedPins[3].assignedPin      = "13";
GPIO1.associatedPins[3].pin.$assign      = "PA13";

const Board                       = scripting.addModule("/ti/driverlib/Board", {}, false);
Board.peripheral.$assign          = "DEBUGSS";
Board.peripheral.swclkPin.$assign = "PA20";
Board.peripheral.swdioPin.$assign = "PA19";

GPIO2.$name                         = "GPIO_GRP_MOTOR";
GPIO2.associatedPins.create(4);
GPIO2.associatedPins[0].$name       = "PIN_FRONT_RIGHT_A";
GPIO2.associatedPins[0].interruptEn = true;
GPIO2.associatedPins[0].polarity    = "RISE";
GPIO2.associatedPins[0].assignedPin = "28";
GPIO2.associatedPins[0].pin.$assign = "PA28";
GPIO2.associatedPins[1].$name       = "PIN_FRONT_LEFT_A";
GPIO2.associatedPins[1].interruptEn = true;
GPIO2.associatedPins[1].polarity    = "RISE";
GPIO2.associatedPins[1].assignedPin = "7";
GPIO2.associatedPins[1].pin.$assign = "PA7";
GPIO2.associatedPins[2].$name       = "PIN_FRONT_RIGHT_B";
GPIO2.associatedPins[2].assignedPin = "31";
GPIO2.associatedPins[2].pin.$assign = "PA31";
GPIO2.associatedPins[3].$name       = "PIN_FRONT_LEFT_B";
GPIO2.associatedPins[3].assignedPin = "21";
GPIO2.associatedPins[3].pin.$assign = "PA21";

GPIO3.$name                         = "GPIO_GRP_ENCODER";
GPIO3.port                          = "PORTB";
GPIO3.associatedPins.create(4);
GPIO3.associatedPins[0].$name       = "PIN_E1A";
GPIO3.associatedPins[0].direction   = "INPUT";
GPIO3.associatedPins[0].interruptEn = true;
GPIO3.associatedPins[0].polarity    = "RISE";
GPIO3.associatedPins[0].assignedPin = "18";
GPIO3.associatedPins[0].pin.$assign = "PB18";
GPIO3.associatedPins[1].$name       = "PIN_E1B";
GPIO3.associatedPins[1].direction   = "INPUT";
GPIO3.associatedPins[1].assignedPin = "19";
GPIO3.associatedPins[1].pin.$assign = "PB19";
GPIO3.associatedPins[2].$name       = "PIN_E2A";
GPIO3.associatedPins[2].direction   = "INPUT";
GPIO3.associatedPins[2].interruptEn = true;
GPIO3.associatedPins[2].polarity    = "RISE";
GPIO3.associatedPins[2].assignedPin = "20";
GPIO3.associatedPins[2].pin.$assign = "PB20";
GPIO3.associatedPins[3].$name       = "PIN_E2B";
GPIO3.associatedPins[3].direction   = "INPUT";
GPIO3.associatedPins[3].assignedPin = "24";
GPIO3.associatedPins[3].pin.$assign = "PB24";

GPIO4.$name                              = "GPIO_GRP_GRAY";
GPIO4.port                               = "PORTA";
GPIO4.associatedPins.create(8);
GPIO4.associatedPins[0].$name            = "PIN_SENSER_1";
GPIO4.associatedPins[0].direction        = "INPUT";
GPIO4.associatedPins[0].internalResistor = "PULL_UP";
GPIO4.associatedPins[0].assignedPin      = "25";
GPIO4.associatedPins[0].pin.$assign      = "PA25";
GPIO4.associatedPins[1].$name            = "PIN_SENSER_2";
GPIO4.associatedPins[1].direction        = "INPUT";
GPIO4.associatedPins[1].internalResistor = "PULL_UP";
GPIO4.associatedPins[1].assignedPin      = "24";
GPIO4.associatedPins[1].pin.$assign      = "PA24";
GPIO4.associatedPins[2].$name            = "PIN_SENSER_3";
GPIO4.associatedPins[2].direction        = "INPUT";
GPIO4.associatedPins[2].internalResistor = "PULL_UP";
GPIO4.associatedPins[2].assignedPin      = "23";
GPIO4.associatedPins[2].pin.$assign      = "PA23";
GPIO4.associatedPins[3].$name            = "PIN_SENSER_4";
GPIO4.associatedPins[3].direction        = "INPUT";
GPIO4.associatedPins[3].internalResistor = "PULL_UP";
GPIO4.associatedPins[3].assignedPin      = "22";
GPIO4.associatedPins[3].pin.$assign      = "PA22";
GPIO4.associatedPins[4].$name            = "PIN_SENSER_5";
GPIO4.associatedPins[4].direction        = "INPUT";
GPIO4.associatedPins[4].internalResistor = "PULL_UP";
GPIO4.associatedPins[4].assignedPin      = "17";
GPIO4.associatedPins[4].pin.$assign      = "PA17";
GPIO4.associatedPins[5].$name            = "PIN_SENSER_6";
GPIO4.associatedPins[5].assignedPin      = "16";
GPIO4.associatedPins[5].pin.$assign      = "PA16";
GPIO4.associatedPins[6].$name            = "PIN_SENSER_7";
GPIO4.associatedPins[6].assignedPin      = "15";
GPIO4.associatedPins[6].pin.$assign      = "PA15";
GPIO4.associatedPins[7].$name            = "PIN_SENSER_8";
GPIO4.associatedPins[7].assignedPin      = "14";
GPIO4.associatedPins[7].pin.$assign      = "PA14";

GPIO5.$name                         = "GPIO_GRP_OLED";
GPIO5.associatedPins.create(2);
GPIO5.associatedPins[0].$name       = "PIN_SCL";
GPIO5.associatedPins[0].pin.$assign = "PA27";
GPIO5.associatedPins[1].$name       = "PIN_SDA";
GPIO5.associatedPins[1].pin.$assign = "PA26";

PWM1.$name                              = "PWM_FRONT_MOTOR";
PWM1.timerCount                         = 3200;
PWM1.pwmMode                            = "EDGE_ALIGN_UP";
PWM1.peripheral.$assign                 = "TIMA1";
PWM1.peripheral.ccp0Pin.$assign         = "PB2";
PWM1.peripheral.ccp1Pin.$assign         = "PB3";
PWM1.PWM_CHANNEL_0.$name                = "ti_driverlib_pwm_PWMTimerCC0";
PWM1.PWM_CHANNEL_1.$name                = "ti_driverlib_pwm_PWMTimerCC1";
PWM1.ccp0PinConfig.direction            = scripting.forceWrite("OUTPUT");
PWM1.ccp0PinConfig.hideOutputInversion  = scripting.forceWrite(false);
PWM1.ccp0PinConfig.onlyInternalResistor = scripting.forceWrite(false);
PWM1.ccp0PinConfig.passedPeripheralType = scripting.forceWrite("Digital");
PWM1.ccp0PinConfig.$name                = "ti_driverlib_gpio_GPIOPinGeneric2";
PWM1.ccp1PinConfig.direction            = scripting.forceWrite("OUTPUT");
PWM1.ccp1PinConfig.hideOutputInversion  = scripting.forceWrite(false);
PWM1.ccp1PinConfig.onlyInternalResistor = scripting.forceWrite(false);
PWM1.ccp1PinConfig.passedPeripheralType = scripting.forceWrite("Digital");
PWM1.ccp1PinConfig.$name                = "ti_driverlib_gpio_GPIOPinGeneric3";

SYSCTL.clockTreeEn = true;

SYSTICK.periodEnable    = true;
SYSTICK.period          = 32000;
SYSTICK.interruptEnable = true;

TIMER1.$name              = "TIMER_0";
TIMER1.timerClkDiv        = 2;
TIMER1.timerMode          = "PERIODIC";
TIMER1.interrupts         = ["ZERO"];
TIMER1.timerPeriod        = "10 ms";
TIMER1.timerClkPrescale   = 16;
TIMER1.peripheral.$assign = "TIMA0";

UART1.$name                            = "UART_DEBUG";
UART1.targetBaudRate                   = 115200;
UART1.rxFifoThreshold                  = "DL_UART_RX_FIFO_LEVEL_ONE_ENTRY";
UART1.enableDMARX                      = false;
UART1.enableDMATX                      = false;
UART1.enabledInterrupts                = ["RX"];
UART1.peripheral.$assign               = "UART0";
UART1.peripheral.rxPin.$assign         = "PA1";
UART1.peripheral.txPin.$assign         = "PA0";
UART1.txPinConfig.direction            = scripting.forceWrite("OUTPUT");
UART1.txPinConfig.hideOutputInversion  = scripting.forceWrite(false);
UART1.txPinConfig.onlyInternalResistor = scripting.forceWrite(false);
UART1.txPinConfig.passedPeripheralType = scripting.forceWrite("Digital");
UART1.txPinConfig.$name                = "ti_driverlib_gpio_GPIOPinGeneric0";
UART1.rxPinConfig.hideOutputInversion  = scripting.forceWrite(false);
UART1.rxPinConfig.onlyInternalResistor = scripting.forceWrite(false);
UART1.rxPinConfig.passedPeripheralType = scripting.forceWrite("Digital");
UART1.rxPinConfig.$name                = "ti_driverlib_gpio_GPIOPinGeneric1";

UART2.$name                            = "UART_JY61P";
UART2.enabledInterrupts                = ["RX"];
UART2.peripheral.$assign               = "UART1";
UART2.peripheral.rxPin.$assign         = "PA9";
UART2.peripheral.txPin.$assign         = "PA8";
UART2.txPinConfig.direction            = scripting.forceWrite("OUTPUT");
UART2.txPinConfig.hideOutputInversion  = scripting.forceWrite(false);
UART2.txPinConfig.onlyInternalResistor = scripting.forceWrite(false);
UART2.txPinConfig.passedPeripheralType = scripting.forceWrite("Digital");
UART2.txPinConfig.$name                = "ti_driverlib_gpio_GPIOPinGeneric6";
UART2.rxPinConfig.hideOutputInversion  = scripting.forceWrite(false);
UART2.rxPinConfig.onlyInternalResistor = scripting.forceWrite(false);
UART2.rxPinConfig.passedPeripheralType = scripting.forceWrite("Digital");
UART2.rxPinConfig.$name                = "ti_driverlib_gpio_GPIOPinGeneric7";

ProjectConfig.deviceSpin = "MSPM0G3507";
