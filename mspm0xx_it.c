// 中断函数文件
#include "ti_msp_dl_config.h"
#include <stdint.h>
#include <mylib/utility/debug.h>
#include <mylib/device/device_motor_encoder.h>
#include <mylib/device/device_jy61p.h>
#include <mylib/device/device_atk_ms53l1m.h>

extern volatile uint32_t g_sysTickUptime;

void SysTick_Handler(void)
{
    g_sysTickUptime++;
}

void GROUP0_IRQHandler(void)
{

}

void GROUP1_IRQHandler(void)
{
    //DEBUG_INFO("GROUP1_IRQHandler");
    motor_encoder_irq_handler();
}

void UART0_IRQHandler(void)
{
    switch (DL_UART_Main_getPendingInterrupt(UART_DEBUG_INST)) {
        case DL_UART_MAIN_IIDX_RX:{
         uint8_t gEchoData = DL_UART_Main_receiveData(UART_DEBUG_INST);
            DL_UART_Main_transmitData(UART_DEBUG_INST, gEchoData);
            //DEBUG_INFO("UART0_IRQHandler");
        }
           
            break;
        default:
            break;
    }
}

void UART1_IRQHandler(void)
{
    //DEBUG_INFO("UART1_IRQHandler");
}

void UART2_IRQHandler(void)
{
    //DEBUG_INFO("UART2_IRQHandler");

}

void UART3_IRQHandler(void)
{
    //DEBUG_INFO("UART3_IRQHandler");
    jy61p_irq_handler();
}


