/*
 * Copyright (c) 2023, Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ============ ti_msp_dl_config.h =============
 *  Configured MSPM0 DriverLib module declarations
 *
 *  DO NOT EDIT - This file is generated for the MSPM0G350X
 *  by the SysConfig tool.
 */
#ifndef ti_msp_dl_config_h
#define ti_msp_dl_config_h

#define CONFIG_MSPM0G350X

#if defined(__ti_version__) || defined(__TI_COMPILER_VERSION__)
#define SYSCONFIG_WEAK __attribute__((weak))
#elif defined(__IAR_SYSTEMS_ICC__)
#define SYSCONFIG_WEAK __weak
#elif defined(__GNUC__)
#define SYSCONFIG_WEAK __attribute__((weak))
#endif

#include <ti/devices/msp/msp.h>
#include <ti/driverlib/driverlib.h>
#include <ti/driverlib/m0p/dl_core.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  ======== SYSCFG_DL_init ========
 *  Perform all required MSP DL initialization
 *
 *  This function should be called once at a point before any use of
 *  MSP DL.
 */


/* clang-format off */

#define POWER_STARTUP_DELAY                                                (16)



#define CPUCLK_FREQ                                                     32000000



/* Defines for PWM_FRONT_MOTOR */
#define PWM_FRONT_MOTOR_INST                                               TIMA1
#define PWM_FRONT_MOTOR_INST_IRQHandler                         TIMA1_IRQHandler
#define PWM_FRONT_MOTOR_INST_INT_IRQN                           (TIMA1_INT_IRQn)
#define PWM_FRONT_MOTOR_INST_CLK_FREQ                                   32000000
/* GPIO defines for channel 0 */
#define GPIO_PWM_FRONT_MOTOR_C0_PORT                                       GPIOB
#define GPIO_PWM_FRONT_MOTOR_C0_PIN                                DL_GPIO_PIN_2
#define GPIO_PWM_FRONT_MOTOR_C0_IOMUX                            (IOMUX_PINCM15)
#define GPIO_PWM_FRONT_MOTOR_C0_IOMUX_FUNC             IOMUX_PINCM15_PF_TIMA1_CCP0
#define GPIO_PWM_FRONT_MOTOR_C0_IDX                          DL_TIMER_CC_0_INDEX
/* GPIO defines for channel 1 */
#define GPIO_PWM_FRONT_MOTOR_C1_PORT                                       GPIOB
#define GPIO_PWM_FRONT_MOTOR_C1_PIN                                DL_GPIO_PIN_3
#define GPIO_PWM_FRONT_MOTOR_C1_IOMUX                            (IOMUX_PINCM16)
#define GPIO_PWM_FRONT_MOTOR_C1_IOMUX_FUNC             IOMUX_PINCM16_PF_TIMA1_CCP1
#define GPIO_PWM_FRONT_MOTOR_C1_IDX                          DL_TIMER_CC_1_INDEX



/* Defines for TIMER_0 */
#define TIMER_0_INST                                                     (TIMA0)
#define TIMER_0_INST_IRQHandler                                 TIMA0_IRQHandler
#define TIMER_0_INST_INT_IRQN                                   (TIMA0_INT_IRQn)
#define TIMER_0_INST_LOAD_VALUE                                          (9999U)



/* Defines for UART_DEBUG */
#define UART_DEBUG_INST                                                    UART0
#define UART_DEBUG_INST_IRQHandler                              UART0_IRQHandler
#define UART_DEBUG_INST_INT_IRQN                                  UART0_INT_IRQn
#define GPIO_UART_DEBUG_RX_PORT                                            GPIOA
#define GPIO_UART_DEBUG_TX_PORT                                            GPIOA
#define GPIO_UART_DEBUG_RX_PIN                                     DL_GPIO_PIN_1
#define GPIO_UART_DEBUG_TX_PIN                                     DL_GPIO_PIN_0
#define GPIO_UART_DEBUG_IOMUX_RX                                  (IOMUX_PINCM2)
#define GPIO_UART_DEBUG_IOMUX_TX                                  (IOMUX_PINCM1)
#define GPIO_UART_DEBUG_IOMUX_RX_FUNC                   IOMUX_PINCM2_PF_UART0_RX
#define GPIO_UART_DEBUG_IOMUX_TX_FUNC                   IOMUX_PINCM1_PF_UART0_TX
#define UART_DEBUG_BAUD_RATE                                            (115200)
#define UART_DEBUG_IBRD_32_MHZ_115200_BAUD                                  (17)
#define UART_DEBUG_FBRD_32_MHZ_115200_BAUD                                  (23)
/* Defines for UART_JY61P */
#define UART_JY61P_INST                                                    UART1
#define UART_JY61P_INST_IRQHandler                              UART1_IRQHandler
#define UART_JY61P_INST_INT_IRQN                                  UART1_INT_IRQn
#define GPIO_UART_JY61P_RX_PORT                                            GPIOA
#define GPIO_UART_JY61P_TX_PORT                                            GPIOA
#define GPIO_UART_JY61P_RX_PIN                                     DL_GPIO_PIN_9
#define GPIO_UART_JY61P_TX_PIN                                     DL_GPIO_PIN_8
#define GPIO_UART_JY61P_IOMUX_RX                                 (IOMUX_PINCM20)
#define GPIO_UART_JY61P_IOMUX_TX                                 (IOMUX_PINCM19)
#define GPIO_UART_JY61P_IOMUX_RX_FUNC                  IOMUX_PINCM20_PF_UART1_RX
#define GPIO_UART_JY61P_IOMUX_TX_FUNC                  IOMUX_PINCM19_PF_UART1_TX
#define UART_JY61P_BAUD_RATE                                              (9600)
#define UART_JY61P_IBRD_32_MHZ_9600_BAUD                                   (208)
#define UART_JY61P_FBRD_32_MHZ_9600_BAUD                                    (21)





/* Defines for PIN_BEEP: GPIOB.9 with pinCMx 26 on package pin 23 */
#define GPIO_GRP_MISC_PIN_BEEP_PORT                                      (GPIOB)
#define GPIO_GRP_MISC_PIN_BEEP_PIN                               (DL_GPIO_PIN_9)
#define GPIO_GRP_MISC_PIN_BEEP_IOMUX                             (IOMUX_PINCM26)
/* Defines for PIN_LED: GPIOB.6 with pinCMx 23 on package pin 20 */
#define GPIO_GRP_MISC_PIN_LED_PORT                                       (GPIOB)
#define GPIO_GRP_MISC_PIN_LED_PIN                                (DL_GPIO_PIN_6)
#define GPIO_GRP_MISC_PIN_LED_IOMUX                              (IOMUX_PINCM23)
/* Defines for PIN_KEY_1: GPIOA.12 with pinCMx 34 on package pin 27 */
#define GPIO_GRP_MISC_PIN_KEY_1_PORT                                     (GPIOA)
#define GPIO_GRP_MISC_PIN_KEY_1_PIN                             (DL_GPIO_PIN_12)
#define GPIO_GRP_MISC_PIN_KEY_1_IOMUX                            (IOMUX_PINCM34)
/* Defines for PIN_KEY_2: GPIOA.13 with pinCMx 35 on package pin 28 */
#define GPIO_GRP_MISC_PIN_KEY_2_PORT                                     (GPIOA)
#define GPIO_GRP_MISC_PIN_KEY_2_PIN                             (DL_GPIO_PIN_13)
#define GPIO_GRP_MISC_PIN_KEY_2_IOMUX                            (IOMUX_PINCM35)
/* Port definition for Pin Group GPIO_GRP_MOTOR */
#define GPIO_GRP_MOTOR_PORT                                              (GPIOA)

/* Defines for PIN_FRONT_RIGHT_A: GPIOA.28 with pinCMx 3 on package pin 3 */
#define GPIO_GRP_MOTOR_PIN_FRONT_RIGHT_A_PIN                    (DL_GPIO_PIN_28)
#define GPIO_GRP_MOTOR_PIN_FRONT_RIGHT_A_IOMUX                    (IOMUX_PINCM3)
/* Defines for PIN_FRONT_LEFT_A: GPIOA.7 with pinCMx 14 on package pin 13 */
#define GPIO_GRP_MOTOR_PIN_FRONT_LEFT_A_PIN                      (DL_GPIO_PIN_7)
#define GPIO_GRP_MOTOR_PIN_FRONT_LEFT_A_IOMUX                    (IOMUX_PINCM14)
/* Defines for PIN_FRONT_RIGHT_B: GPIOA.31 with pinCMx 6 on package pin 5 */
#define GPIO_GRP_MOTOR_PIN_FRONT_RIGHT_B_PIN                    (DL_GPIO_PIN_31)
#define GPIO_GRP_MOTOR_PIN_FRONT_RIGHT_B_IOMUX                    (IOMUX_PINCM6)
/* Defines for PIN_FRONT_LEFT_B: GPIOA.21 with pinCMx 46 on package pin 39 */
#define GPIO_GRP_MOTOR_PIN_FRONT_LEFT_B_PIN                     (DL_GPIO_PIN_21)
#define GPIO_GRP_MOTOR_PIN_FRONT_LEFT_B_IOMUX                    (IOMUX_PINCM46)
/* Port definition for Pin Group GPIO_GRP_ENCODER */
#define GPIO_GRP_ENCODER_PORT                                            (GPIOB)

/* Defines for PIN_E1A: GPIOB.18 with pinCMx 44 on package pin 37 */
// pins affected by this interrupt request:["PIN_E1A","PIN_E2A"]
#define GPIO_GRP_ENCODER_INT_IRQN                               (GPIOB_INT_IRQn)
#define GPIO_GRP_ENCODER_INT_IIDX               (DL_INTERRUPT_GROUP1_IIDX_GPIOB)
#define GPIO_GRP_ENCODER_PIN_E1A_IIDX                       (DL_GPIO_IIDX_DIO18)
#define GPIO_GRP_ENCODER_PIN_E1A_PIN                            (DL_GPIO_PIN_18)
#define GPIO_GRP_ENCODER_PIN_E1A_IOMUX                           (IOMUX_PINCM44)
/* Defines for PIN_E1B: GPIOB.19 with pinCMx 45 on package pin 38 */
#define GPIO_GRP_ENCODER_PIN_E1B_PIN                            (DL_GPIO_PIN_19)
#define GPIO_GRP_ENCODER_PIN_E1B_IOMUX                           (IOMUX_PINCM45)
/* Defines for PIN_E2A: GPIOB.20 with pinCMx 48 on package pin 41 */
#define GPIO_GRP_ENCODER_PIN_E2A_IIDX                       (DL_GPIO_IIDX_DIO20)
#define GPIO_GRP_ENCODER_PIN_E2A_PIN                            (DL_GPIO_PIN_20)
#define GPIO_GRP_ENCODER_PIN_E2A_IOMUX                           (IOMUX_PINCM48)
/* Defines for PIN_E2B: GPIOB.24 with pinCMx 52 on package pin 42 */
#define GPIO_GRP_ENCODER_PIN_E2B_PIN                            (DL_GPIO_PIN_24)
#define GPIO_GRP_ENCODER_PIN_E2B_IOMUX                           (IOMUX_PINCM52)
/* Port definition for Pin Group GPIO_GRP_GRAY */
#define GPIO_GRP_GRAY_PORT                                               (GPIOA)

/* Defines for PIN_SENSER_1: GPIOA.25 with pinCMx 55 on package pin 45 */
#define GPIO_GRP_GRAY_PIN_SENSER_1_PIN                          (DL_GPIO_PIN_25)
#define GPIO_GRP_GRAY_PIN_SENSER_1_IOMUX                         (IOMUX_PINCM55)
/* Defines for PIN_SENSER_2: GPIOA.24 with pinCMx 54 on package pin 44 */
#define GPIO_GRP_GRAY_PIN_SENSER_2_PIN                          (DL_GPIO_PIN_24)
#define GPIO_GRP_GRAY_PIN_SENSER_2_IOMUX                         (IOMUX_PINCM54)
/* Defines for PIN_SENSER_3: GPIOA.23 with pinCMx 53 on package pin 43 */
#define GPIO_GRP_GRAY_PIN_SENSER_3_PIN                          (DL_GPIO_PIN_23)
#define GPIO_GRP_GRAY_PIN_SENSER_3_IOMUX                         (IOMUX_PINCM53)
/* Defines for PIN_SENSER_4: GPIOA.22 with pinCMx 47 on package pin 40 */
#define GPIO_GRP_GRAY_PIN_SENSER_4_PIN                          (DL_GPIO_PIN_22)
#define GPIO_GRP_GRAY_PIN_SENSER_4_IOMUX                         (IOMUX_PINCM47)
/* Defines for PIN_SENSER_5: GPIOA.17 with pinCMx 39 on package pin 32 */
#define GPIO_GRP_GRAY_PIN_SENSER_5_PIN                          (DL_GPIO_PIN_17)
#define GPIO_GRP_GRAY_PIN_SENSER_5_IOMUX                         (IOMUX_PINCM39)
/* Defines for PIN_SENSER_6: GPIOA.16 with pinCMx 38 on package pin 31 */
#define GPIO_GRP_GRAY_PIN_SENSER_6_PIN                          (DL_GPIO_PIN_16)
#define GPIO_GRP_GRAY_PIN_SENSER_6_IOMUX                         (IOMUX_PINCM38)
/* Defines for PIN_SENSER_7: GPIOA.15 with pinCMx 37 on package pin 30 */
#define GPIO_GRP_GRAY_PIN_SENSER_7_PIN                          (DL_GPIO_PIN_15)
#define GPIO_GRP_GRAY_PIN_SENSER_7_IOMUX                         (IOMUX_PINCM37)
/* Defines for PIN_SENSER_8: GPIOA.14 with pinCMx 36 on package pin 29 */
#define GPIO_GRP_GRAY_PIN_SENSER_8_PIN                          (DL_GPIO_PIN_14)
#define GPIO_GRP_GRAY_PIN_SENSER_8_IOMUX                         (IOMUX_PINCM36)
/* Port definition for Pin Group GPIO_GRP_OLED */
#define GPIO_GRP_OLED_PORT                                               (GPIOA)

/* Defines for PIN_SCL: GPIOA.27 with pinCMx 60 on package pin 47 */
#define GPIO_GRP_OLED_PIN_SCL_PIN                               (DL_GPIO_PIN_27)
#define GPIO_GRP_OLED_PIN_SCL_IOMUX                              (IOMUX_PINCM60)
/* Defines for PIN_SDA: GPIOA.26 with pinCMx 59 on package pin 46 */
#define GPIO_GRP_OLED_PIN_SDA_PIN                               (DL_GPIO_PIN_26)
#define GPIO_GRP_OLED_PIN_SDA_IOMUX                              (IOMUX_PINCM59)



/* clang-format on */

void SYSCFG_DL_init(void);
void SYSCFG_DL_initPower(void);
void SYSCFG_DL_GPIO_init(void);
void SYSCFG_DL_SYSCTL_init(void);
void SYSCFG_DL_PWM_FRONT_MOTOR_init(void);
void SYSCFG_DL_TIMER_0_init(void);
void SYSCFG_DL_UART_DEBUG_init(void);
void SYSCFG_DL_UART_JY61P_init(void);

void SYSCFG_DL_SYSTICK_init(void);

bool SYSCFG_DL_saveConfiguration(void);
bool SYSCFG_DL_restoreConfiguration(void);

#ifdef __cplusplus
}
#endif

#endif /* ti_msp_dl_config_h */
